package tictactoe;

class Board {
	private Tile[][] tiles;

	public Board() {
		tiles = new Tile[3][3];

		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 3; column++) {
				tiles[row][column] = new Tile();
			}
		}
	}

	public Tile getTile(int row, int column) {
		Tile result = tiles[row][column];
		return result;
	}
	
	public boolean isFull() {
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 3; column ++) {
				if (tiles[row][column].getOccupant().getCharacterRepresentation() == ' ') {
					return false;
				}
			}
		}
		
		return true;
	}

	public boolean movePlayer(Player player, Tile tile) {
		if (tile.isOccupiedBy(' ')) {
			tile.setOccupant(player);
			return true;
		} else {
			return false;
		}
	}
}
