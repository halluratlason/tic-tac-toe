package tictactoe;

class Game {
	private Board board;
	private Player player1;
	private Player player2;
	private Player currentPlayer;

	public Board getBoard() {
		return board;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public Game() {
		board = new Board();
		player1 = new Player('x');
		player2 = new Player('o');
		currentPlayer = player1;
	}
	
	public boolean isWinner(Player player) {
				 // 1st row
		return ((board.getTile(0, 0).isOccupiedBy(player) &&
				 board.getTile(0, 1).isOccupiedBy(player) &&
				 board.getTile(0, 2).isOccupiedBy(player)) ||
				 // 2nd row
				(board.getTile(1, 0).isOccupiedBy(player) &&
				 board.getTile(1, 1).isOccupiedBy(player) &&
				 board.getTile(1, 2).isOccupiedBy(player)) ||
				 // 3rd row
				(board.getTile(2, 0).isOccupiedBy(player) &&
				 board.getTile(2, 1).isOccupiedBy(player) &&
				 board.getTile(2, 2).isOccupiedBy(player)) ||
				 // 1st column
				(board.getTile(0, 0).isOccupiedBy(player) &&
			 	 board.getTile(1, 0).isOccupiedBy(player) &&
				 board.getTile(2, 0).isOccupiedBy(player)) ||
				 // 2nd column
				(board.getTile(0, 1).isOccupiedBy(player) &&
				 board.getTile(1, 1).isOccupiedBy(player) &&
				 board.getTile(2, 1).isOccupiedBy(player)) ||
				 // 3rd column
				(board.getTile(0, 2).isOccupiedBy(player) &&
				 board.getTile(1, 2).isOccupiedBy(player) &&
				 board.getTile(2, 2).isOccupiedBy(player)) ||
				 // Top left to button right
				(board.getTile(0, 0).isOccupiedBy(player) &&
				 board.getTile(1, 1).isOccupiedBy(player) &&
				 board.getTile(2, 2).isOccupiedBy(player)) ||
				 // Top right to bottom left
				(board.getTile(0, 2).isOccupiedBy(player) &&
				 board.getTile(1, 1).isOccupiedBy(player) &&
				 board.getTile(2, 0).isOccupiedBy(player)));
	}
	
	public void switchCurrentPlayer() {
		currentPlayer = ((currentPlayer == player1) ? player2 : player1);
	}
		
	public static void main(String[] args) {
		Game game = new Game();
		View view = new View();
		
		while (true) {
			view.printBoard(game.getBoard());
			game.getCurrentPlayer().turn(game.getBoard());
			
			if (game.isWinner(game.getCurrentPlayer())) {
				view.printBoard(game.getBoard());
				view.printLine("Player " + game.getCurrentPlayer().getCharacterRepresentation() + " won!");
				break;
			} else if (game.getBoard().isFull()) {
				view.printBoard(game.getBoard());
				view.printLine("It's a draw!");
				break;
			} else {
				game.switchCurrentPlayer();
			}
		}
	}
}
