package tictactoe;

class Player {
	private char characterRepresentation;

	public char getCharacterRepresentation() {
		return characterRepresentation;
	}

	public Player(char characterRepresentation) {
		this.characterRepresentation = characterRepresentation;
	}

	public void turn(Board board) {
		View view = new View();
		
		view.printLine("Player " + this.getCharacterRepresentation() + "'s turn.");
		
		while (true) {
			int row = view.getValidMove("Row [1, 3]: ");
			int column = view.getValidMove("Column [1, 3]: ");

			Tile targetTile = board.getTile(row, column);

			if (board.movePlayer(this, targetTile)) {
				break;
			} else {
				view.printLine("That tiles is already taken.");
			}
		 }
	}
}
