package tictactoe;

class Tile{
	private Player occupant;

	public Player getOccupant(){
		return occupant;
	}
	public void setOccupant(Player occupant){
		this.occupant = occupant;
	}

	public Tile() {
		occupant = new Player(' ');
	}

	public boolean isOccupiedBy(Player player) {
		return ((occupant.getCharacterRepresentation() == player.getCharacterRepresentation()));
	}

	public boolean isOccupiedBy(char playerCharacterRepresentation) {
		return ((occupant.getCharacterRepresentation() == playerCharacterRepresentation));
	}

	public String toString() {
		return Character.toString(occupant.getCharacterRepresentation());
	}
}
