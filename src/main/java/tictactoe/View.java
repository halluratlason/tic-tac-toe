package tictactoe;

import java.util.Scanner;

class View {
	public void printBoard(Board board) {
		printLine("    1   2   3");
		printLine("");
		printLine("1   " + board.getTile(0,  0) + " | " + board.getTile(0,  1) + " | " + board.getTile(0,  2) + " ");
		printLine("   -----------");
		printLine("2   " + board.getTile(1,  0) + " | " + board.getTile(1,  1) + " | " + board.getTile(1,  2) + " ");
		printLine("   -----------");
		printLine("3   " + board.getTile(2,  0) + " | " + board.getTile(2,  1) + " | " + board.getTile(2,  2) + " ");
		
	}
	
	public void printLine(String str) {
		System.out.println(str);
	}

	public void print(String str) {
		System.out.print(str);
	}

	public int getIntInput() {
		Scanner scanner = new Scanner(System.in);
		int intInput = scanner.nextInt();
		
		return intInput;
	}
	
	private boolean validateMove(int moveInput) {
		return (moveInput >= 0 && moveInput <= 2);
	}

	public int getValidMove(String message) {
		int move;
		while (true) {
			print(message);
			move = (getIntInput() - 1);
			
			if (validateMove(move)) {
				return move;
			} else {
				printLine("Invalid input.");
			}
		}
	}
}
